# Rapport TP2 IDM

## Question 3

Commande à exécuter:
```bash
./run.sh q3
```

Résultat:
```bash
rm -rf build/* generator exe
clang mt.c -c -o build/mt.o -O2
clang generate.c build/mt.o -o generator -O2
clang build/data.c -c -o build/data.o -O2
clang main.c -c -o build/main.o -O2
clang build/data.o build/main.o build/mt.o -o exe -O2
========== With generation on the go ==========
0         : 3.144760  
1         : 3.139896  
2         : 3.142428  
3         : 3.142288  
4         : 3.142120  
5         : 3.139728  
6         : 3.139852  
7         : 3.140296  
8         : 3.139300  
9         : 3.142188  
Average difference: -0.000307

real    0m0,123s
user    0m0,121s
sys     0m0,001s
```

Avec un niveau d'optimisation `-O2`, on observe un temps d'exécution de `0,123s`, avec un équart moyen de `0.000307`.

## Question 4

Commande à exécuter:
```bash
./run.sh q4
```

Résultat:
```bash
rm -rf build/* generator exe
clang mt.c -c -o build/mt.o -O2
clang generate.c build/mt.o -o generator -O2
clang build/data.c -c -o build/data.o -O2
clang main.c -c -o build/main.o -O2
clang build/data.o build/main.o build/mt.o -o exe -O2

real    0m1,284s
user    0m0,691s
sys     0m0,576s
========== With numbers already generated ==========
0         : 3.144760  
1         : 3.139896  
2         : 3.142428  
3         : 3.142288  
4         : 3.142120  
5         : 3.139728  
6         : 3.139852  
7         : 3.140296  
8         : 3.139300  
9         : 3.142188  
Average difference: -0.000307

real    0m0,040s
user    0m0,026s
sys     0m0,014s
```

En compilant sur `turing` avec le même niveau d'optimisation que pour la `question 3`, on observe un temps de compilation de `1,284s`, pour un temps d'exécution de `0,040s`.

## Question 5

Commande à exécuter:
```bash
./run.sh q5
```

Résultat:
```bash
rm -rf build/* generator exe
clang mt.c -c -o build/mt.o -O2
clang generate.c build/mt.o -o generator -O2

real    0m38,821s
user    0m36,063s
sys     0m2,676s
clang build/data.c -c -o build/data.o -O2
clang build/data.o build/main.o build/mt.o -o exe -O2

real    12m39,714s
user    11m44,541s
sys     0m53,345s
========== With generation on the go ==========
0         : 3.141286  
1         : 3.140988  
2         : 3.141154  
3         : 3.141318  
4         : 3.140341  
5         : 3.140754  
6         : 3.141517  
7         : 3.141630  
8         : 3.141908  
9         : 3.141558  
Average difference: -0.000347

real    0m1,016s
user    0m1,013s
sys     0m0,001s
========== With numbers already generated ==========
0         : 3.141286  
1         : 3.140988  
2         : 3.141154  
3         : 3.141318  
4         : 3.140340  
5         : 3.140754  
6         : 3.141517  
7         : 3.141630  
8         : 3.141908  
9         : 3.141558  
Average difference: -0.000347

real    0m0,337s
user    0m0,173s
sys     0m0,163s
```

On remarque que pour générer effectuer une estimation de `pi` avec un total de `200 000 000` nombres, la méthode qui constite à générer les nombres à la volée prend `1,013s`, contre `0,337s` pour celle utilisant les nombres générés au préalable. Cependant, on remarque que le fichier contenant le tableau de nombres pré-générés demande un temps de compilation de `12m 39,714s`, ce qui est assez long comparé au temps d'exécution, et le fichier `.o` est assez lourd.

## Question 6

Commande à exécuter:
```bash
./run.sh q6
```

Résultat:
```bash
rm -rf build/* generator exe
clang mt.c -c -o build/mt.o -O2
clang generate.c build/mt.o -o generator -O2
clang build/data.c -c -o build/data.o -O2
clang main.c -c -o build/main.o -O2
clang build/data.o build/main.o build/mt.o -o exe -O2
========== With generation on the go ==========
0         : 3.141546  
Average difference: -0.000047

real    0m10,894s
user    0m10,850s
sys     0m0,003s
```

Il resterait envisigeable, bien que compliqué, de générer 2 milliards de points à l'avance dans un fichier. Cependant, pour 20 milliards de points, cela prendrait plusieurs heures et le fichier généré ferait plusieurs centaines de Go, ce qui devient presque impossible avec les contraintes matérielles sur `turing`.