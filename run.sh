#!/bin/bash

if [[ -z ${1} ]]; then
    echo "Usage: ./run.sh (q3|q4|q5|q6)"
fi

mkdir -p build

if [[ "${1}" == "q3" ]]; then
    make clean
    make generator
    ./generator 20000000
    make exe
    echo "========== With generation on the go =========="
    time ./exe 1 1000000 10
fi

if [[ "${1}" == "q4" ]]; then
    make clean
    make generator
    ./generator 20000000
    time make exe
    echo "========== With numbers already generated =========="
    time ./exe 2 1000000 10
fi

if [[ "${1}" == "q5" ]]; then
    make clean
    make generator
    time ./generator 200000000
    time make exe
    echo "========== With generation on the go =========="
    time ./exe 1 10000000 10
    echo "========== With numbers already generated =========="
    time ./exe 2 10000000 10
fi

if [[ "${1}" == "q6" ]]; then
    make clean
    make generator
    ./generator 0
    make exe
    echo "========== With generation on the go =========="
    time ./exe 1 1000000000 1
fi
