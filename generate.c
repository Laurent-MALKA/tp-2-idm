#include <float.h>
#include <stdio.h>
#include <stdlib.h>

#include "mt.h"

typedef unsigned long long int ull;

int main(int argc, char** argv) {
    if (argc != 2) {
        return 1;
    }
    FILE* output = fopen("build/data.c", "w");
    const ull nb_of_points_to_generate = atoi(argv[1]);
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    fprintf(output, "const unsigned long long int MAX_ITERATIONS = %llu;",
            nb_of_points_to_generate);
    fputs("const double TAB[] = {", output);
    for (ull i = 0; i < nb_of_points_to_generate; ++i) {
        fprintf(output, "%.*e,", FLT_DIG, genrand_real1());
    }
    fputs("};", output);

    fclose(output);

    return 0;
}
